﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehicles
{
	class Car : Vehicle // Car IS-A Vehicle
	{
		public Car(string make) : base(make)
		{ 
		}

		public override void Go()
		{
			Console.WriteLine("Vruummmmm goes the car");
		}
	}
}
